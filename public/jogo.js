
// ----------------------------------------------------------------- //
//                            CONFIG
// ----------------------------------------------------------------- //

let game_width = 800;
let game_height = 600;

// colors ----------------------------------------------------------

let linha_red            = '#9a0c0c';
let linha_red_light      = '#f30c0c';

let stats_light      = '#FFF';
let stats_mid        = '#999';
let stats_dark       = '#444';

/*
// not using the brand colors for anything yet

let australis_light  = '#00ac9f';
let australis_mid    = '#306fb7';
let australis_dark   = '#002b51';

let adani_light      = '#007ba6';
let adani_mid        = '#bf3a59';
let adani_dark       = '#773295';
// #3c4fa6
// #2b3877

let exxon_light      = '#ed1b2d';
let exxon_mid        = '#bfbfbf';
let exxon_dark       = '#565656';

// todo
let total_light      = '#00ac9f';
let total_mid        = '#306fb7';
let total_dark       = '#002b51';
*/


let brand;
let cena_location;
let money;

// let background_color     = '#E6A774';
// let sand                 = '#c7a953';
// let green_light          = '#3B7845';
// let green_dark           = '#014819';

switch(level) {
   case 1:
      brand = 'adani';
      money = '$';
      cena_location = 'queensland';
      break;
   case 2:
      brand = 'exxon';
      money = '$';
      cena_location = 'vaca_muerta';
      break;
   case 3:
      brand = 'total';
      money = '€';
      cena_location = 'cabo_delgado';
      break;
}

// gameplay --------------------------------------------------------

let linha_init = 25;  // initial linha qty
let bolsa_init;       // initial bolsa qty

switch(level) {
   case 1:
   case 2:
      bolsa_init = 1000;
      break;
   case 3:
      bolsa_init = 1300;
      break;
}

let linha_total = linha_init;

let tick = 2000; // cost update interval millis

let end_timer;
let end_timeout = 1500; // pause before win/lose popup
let game_end = false;

// map coordinates -------------------------------------------------

let start_x = 950;
let start_y = 950;

let batalha_x = 705;
let batalha_y = 1294;

let pombal_x = 1014;
let pombal_y = 535;


// decor -----------------------------------------------------------

let novelo_x = 70;
let novelo_y = 75;
let pasta_x = 85; // ( = width - pasta_X )
let pasta_y = 80;

const num_cenas = 6;  // number of different props available
const cena_count = 7; // number of props to draw

// scenery element sizes
const cena_size = [73, 90, 60, 50, 66, 50, 63];

// scale factor for all sprite graphics
const sprite_scale = 0.5;


// sprites ---------------------------------------------------------

let suit_timer_init = -10000;
let dozer_timer_init = 10000;
let digger_timer_init = 15000;
let tanker_timer_init = 15000;
let suit_interval = 5000;
let dozer_interval = 15000;
let digger_interval = 20000;
let tanker_interval = 20000;

let suit_slowdown = 0.2; // affects maxSpeed
let suit_max_speed = 0.7;
let suit_cost_rate = 2; // "cost per tick"
let suit_cost_max = 20;
let suit_width = 20;
let suit_height = 30;
let suit_turn_timeout = 1000;

let dozer_slowdown = 0.1;
let dozer_max_speed = 0.4;
let dozer_cost_rate = 3;
let dozer_cost_max = 50;
let dozer_size = 35;
let dozer_turn_timeout = 1000;

let digger_slowdown = 0.15;
let digger_max_speed = 0.6;
let digger_cost_rate = 5;
let digger_cost_max = 80;
let digger_size = 45;
let digger_turn_timeout = 1000;

let tanker_slowdown = 0.04;
let tanker_max_speed = 0.2;
let tanker_cost_rate = 8;
let tanker_cost_max = 150;
let tanker_size = 55;
let tanker_turn_timeout = 1000;


// ----------------------------------------------------------------- //
//                               VARS
// ----------------------------------------------------------------- //

// game graphics buffer
let pg;

let font;

let game_state = 0;
/*
   0 = welcome
   1 = playing
   2 = lost
   3 = won
*/

// avialable linha
let linha = linha_total;

// remaining bolsa
let bolsa = bolsa_init;

// sprite groups
let target;
let cenas;
let suits;
let dozers;
let diggers;
let tankers;

// sprite timers
let suit_timer;
let dozer_timer;
let digger_timer;
let tanker_timer;

// sprite graphics
// each skin has four directions, with one skin and four wrap overlays
let dozer_gfx = Array.from(Array(4), () => new Array(5));
let digger_gfx = Array.from(Array(4), () => new Array(5));
let tanker_gfx = Array.from(Array(4), () => new Array(5));
let suit_gfx = Array.from(Array(4), () => new Array(6)); // suits have two walking frames

// decor graphics
let target_gfx;
let target_working;
let target_blocked;
let novelo_gfx = new Array(5);
let pasta_gfx = new Array(7);
let cena_gfx = new Array(num_cenas);


// ----------------------------------------------------------------- //
//                          SETUP & PRELOAD
// ----------------------------------------------------------------- //

function preload() {

   font = loadFont('assets/AnonymousPro-Bold.ttf');

   target_gfx = loadImage('assets/target/' + brand + '/target.png');
   target_working = loadImage('assets/target/' + brand + '/target_working.png');
   target_blocked = loadImage('assets/target/' + brand + '/target_blocked.png');

   for(let i=1; i<=novelo_gfx.length; i++) {
      novelo_gfx[i-1] = loadImage('assets/linha/linha' + i + '.png');
   }
   for(let i=1; i<=pasta_gfx.length; i++) {
      pasta_gfx[i-1] = loadImage('assets/pasta/' + brand + '/pasta' + i + '.png');
   }
   for(let i=1; i<=cena_gfx.length; i++) {
      cena_gfx[i-1] = loadImage('assets/cena/' + cena_location + '/cena' + i + '.png');
   }

   let dir = ['nw', 'ne', 'se', 'sw'];

   for(let i=0; i<4; i++) {
      d = dir[i];
      digger_gfx[i][4] = loadImage('assets/digger/' + brand + '/digger_' + d + '.png');
      dozer_gfx[i][4]  = loadImage('assets/dozer/' + brand + '/dozer_'   + d + '.png');
      if(level > 2) {
         tanker_gfx[i][4] = loadImage('assets/tanker/' + brand + '/tanker_' + d + '.png');
      }
      suit_gfx[i][4]   = loadImage('assets/suit/' + brand + '/suit_'     + d + '1.png');
      suit_gfx[i][5]   = loadImage('assets/suit/' + brand + '/suit_'     + d + '2.png');

      for(let j=1; j<=4; j++) {
         digger_gfx[i][j-1] = loadImage('assets/digger/digger_' + d + '_wrap' + j + '.png');
         dozer_gfx[i][j-1]  = loadImage('assets/dozer/dozer_'   + d + '_wrap' + j + '.png');
         if(level > 2) {
            tanker_gfx[i][j-1] = loadImage('assets/tanker/tanker_' + d + '_wrap' + j + '.png');
         }
         suit_gfx[i][j-1]   = loadImage('assets/suit/suit_'     + d + '_wrap' + j + '.png');
      }
   }
}


function setup() {

   let myCanvas = createCanvas(game_width, game_height);
   myCanvas.parent('jogo');
   $("#bem_vindo").attr("style","display: ''");
   $("#loading").attr("style","display: none");

   pg = createGraphics(game_width, game_height);
   pg.strokeWeight(2);

   target = pg.createSprite(pg.width/2, pg.height/2, 50, 50);
   target.setCollider("circle");
   target.draw = function() {
      // pg.fill(sand);
      // pg.ellipse(0,0,this.width*2, this.height*2);
      // pg.fill(100);
      // pg.triangle(-20,20,0,-40,20,20);
      let gfx;
      switch(game_state) {
         case 0:
         case 1:
            gfx = target_gfx;
            break;
         case 2:
            gfx = target_working;
            break;
         case 3:
            gfx = target_blocked;
            break;
      }
      let y = (level == 1) ? -30 : 0;
      pg.image(gfx, 0,y,target_gfx.width * sprite_scale, target_gfx.height * sprite_scale);

   }

   novelo = pg.createSprite(novelo_x, novelo_y, 50, 50);
   novelo.draw = function() {
      let l = round(map(linha, linha_total, 0, 0, novelo_gfx.length-2));
      if(linha <= 0) {
         l = novelo_gfx.length-1;
      }
      pg.image(novelo_gfx[l], 0,0,novelo_gfx[l].width * sprite_scale, novelo_gfx[l].height * sprite_scale);
   }

   pasta = pg.createSprite(pg.width - pasta_x, pasta_y, 50, 50);
   pasta.draw = function() {
      let l = round(map(bolsa, bolsa_init, 0, pasta_gfx.length-1, 1));
      if(bolsa <= 0) {
         l = 0;
      }
      pg.image(pasta_gfx[l], 0,0,pasta_gfx[l].width * sprite_scale, pasta_gfx[l].height * sprite_scale);
   }

   init();
   placeCenas();
}


// ----------------------------------------------------------------- //
//                            MAIN LOOP
// ----------------------------------------------------------------- //

function draw() {

   clear();

   for(var i=0; i<allSprites.length; i++) {
      allSprites[i].depth = allSprites[i].position.y;
   }
   novelo.depth = 1000;
   pasta.depth = 1000;

   pg.clear();

   switch(game_state) {

      case 1: // playing

         if(millis() - suit_timer > suit_interval) {
            suit_timer = millis();
            newSprite('suit');
         }
         if(millis() - dozer_timer > dozer_interval) {
            dozer_timer = millis();
            newSprite('dozer');
         }
         if(millis() - digger_timer > digger_interval && level > 1) {
            digger_timer = millis();
            newSprite('digger');
         }
         if(millis() - tanker_timer > tanker_interval && level > 2) {
            tanker_timer = millis();
            newSprite('tanker');
         }

         for(let i = 0; i<suits.length; i++) {
            suits[i].doBusiness();
         }
         for(let i = 0; i<dozers.length; i++) {
            dozers[i].doBusiness();
         }
         for(let i = 0; i<diggers.length; i++) {
            diggers[i].doBusiness();
         }
         for(let i = 0; i<tankers.length; i++) {
            tankers[i].doBusiness();
         }
         suits.bounce(cenas);
         dozers.bounce(cenas);
         diggers.bounce(cenas);
         tankers.bounce(cenas);

      case 0: // welcome
      case 2: // lost
      case 3: // won
         drawSprites();
         drawStats();
         break;

   }

   if(game_state > 1 && !game_end) {
      if(millis() - end_timer > end_timeout) {
         if(game_state == 2) {
            perdeste();
         } else {
            ganhaste();
         }
         game_end = true;
      }
   }

   image(pg, 0,0);
}


// ----------------------------------------------------------------- //
//                            GAME STATE
// ----------------------------------------------------------------- //

function init() {
   cenas = new Group();
   suits = new Group();
   dozers = new Group();
   diggers = new Group();
   tankers = new Group();
   initCenas();
}

function clearSprites(){
   suits.removeSprites();
   suits.clear();
   dozers.removeSprites();
   dozers.clear();
   diggers.removeSprites();
   diggers.clear();
   tankers.removeSprites();
   tankers.clear();
}

function initTimers() {
   linha_total = linha_init;
   linha = linha_total;
   bolsa = bolsa_init;
   let m = millis();
   suit_timer = m + suit_timer_init;
   dozer_timer = m + dozer_timer_init;
   digger_timer = m + digger_timer_init;
   tanker_timer = m + tanker_timer_init;
   game_end = false;
}

function loseGame() {
   game_state = 2;
   clearSprites();
   end_timer = millis();
}

function winGame() {
   game_state = 3;
   clearSprites();
   end_timer = millis();
}


// ----------------------------------------------------------------- //
//                              SPRITES
// ----------------------------------------------------------------- //

function newSprite(type) {

   let new_sprite = pg.createSprite(0,0, 10,10);
   new_sprite.position = initPosition();
   new_sprite.linha_wrap = 0; // counts the number of times the sprite has been clicked
   new_sprite.novelo_gfx = shuffle(new Array(0,1,2,3)); // random order for applies wrap textures
   new_sprite.cost = 0;
   new_sprite.tick_timer = 0;
   new_sprite.ticker = 0;

   // cardinal direction for choosing graphic
   // 0 nw, 1 ne, 2 se, 3 sw
   new_sprite.dir = 0;
   new_sprite.pdir = 0;

   // angle to tower (not used by suits)
   new_sprite.target_dir = 0;

   // to prevent direction flickering back and forth
   // minimum time between direction changes (set by turn timeout)
   new_sprite.turn_latch = false;
   new_sprite.turn_timer = 0;


   if(type == 'suit') {
      new_sprite.walk_step = 1; // for switching frames on walking animatiosn
      new_sprite.walk_timer = 0;
      new_sprite.walk_time = 200;
   }


   new_sprite.hit = function() {
      if(this.linha_wrap < 4 && linha > 0) {
         linha--;
         this.linha_wrap++;
         this.maxSpeed -= this.slowdown;
         if(this.maxSpeed <= 0.01) {
            this.maxSpeed = 0;
            //this.rotation = 90;
            this.walk_time = 500;
         }
      }
   };

   new_sprite.draw = function() {
      //if(game_state == 1) {
         this.drawBody();

         // let w = this.width;
         // let h = this.height;
         // pg.stroke(linha_red_light);
         // for(let i=0; i<this.linha_wrap; i++) {
         //    pg.line(-(w/2 + 5),i*5,w/2 + 5,i*5 - 10);
         // }

         // ticker
         if(this.ticker > 0) {
            let c = color(stats_light);
            pg.fill(red(c), green(c), blue(c),this.ticker);
            pg.noStroke();
            pg.textSize(30);
            pg.textAlign(CENTER,BOTTOM);
            pg.text(money,0,-0.15*(255-this.ticker) - 10);
            this.ticker -= 5;
            if(this.ticker<0) {
               this.ticker = 0;
            }
         }

         // health bar
         pg.push()
         pg.fill(stats_light);
         pg.rectMode(CENTER);
         let a = this.cost / this.cost_max;
         pg.rect(0,40,(1-a)*this.cost_max,4);

         // pg.noStroke();
         // pg.textSize(14);
         // pg.textStyle(BOLD);
         // pg.textAlign(CENTER,BOTTOM);
         // pg.text(this.cost+'/'+this.cost_max,0,30);
         pg.pop();
      //}
   };

   new_sprite.doBusiness = function() {


      if(!this.turn_latch) {

         // for finding the right direction
         let a;

         if(type == 'suit') {
            // suits just use the attraction point, moving at any angle
            this.attractionPoint(0.2, target.position.x, target.position.y);
            a = this.getDirection();
         } else {
            // vehicles travel only at 45 degrees
            // so we store the direction to the tower in target_dir
            this.target_dir = atan2(
               target.position.x - this.position.x, 
               target.position.y - this.position.y
            );
            // convert to degrees as is used in p5.play
            a = -360 * (this.target_dir - PI/2)/TWO_PI;
            this.setSpeed(this.maxSpeed, a);
         }

         // choose cardinal direction based on angle to tower
         if (a < -180) {
            a += 360;
         }
         if (a >= 180) {
            a -= 360;
         }
         if(a >= -180 && a < -90) {
            this.dir = 0;
         } else if(a >= -90 && a < 0) {
            this.dir = 1;
         } else if(a >= 0 && a < 90) {
            this.dir = 2;
         } else if(a >= 90 && a < 180) {
            this.dir = 3;
         } else {
            this.dir = -1;
         }

         // set movement direction for vehicles based on cardinal direction
         if(type != 'suit') {
            this.setSpeed(this.maxSpeed, 45 + (this.dir*90 - 180));
         }

         // set turn latch when direction changes
         if(this.pdir != this.dir) {
            this.turn_latch = true;
            this.turn_timer = millis();
            this.pdir = this.dir;
         }

      } else {

         let t;
         switch (type) {
            case 'suit':
               t = suit_turn_timeout;
               break;

            case 'dozer':
               t = dozer_turn_timeout;
               break;

            case 'digger':
               t = digger_turn_timeout;
               break;

            case 'tanker':
               t = tanker_turn_timeout;
               break;
         }

         if(millis() - this.turn_timer > t) {
            this.turn_latch = false;
         }

      }

      this.collide(target, loseGame);
      if(millis() - this.tick_timer > tick) {
         this.tick_timer = millis();
         this.ticker = 255;
         this.cost += this.cost_rate;
         bolsa -= this.cost_rate;
         if(bolsa<=0) {
            winGame();
         }
         if(this.cost > this.cost_max) {
            linha += this.linha_wrap;
            this.remove();
         }
      }

      if(type == 'suit') { // && this.linha_wrap < 4) {
         if(millis() - this.walk_timer > this.walk_time) {
            this.walk_step *= -1;
            this.walk_timer = millis();
         }
      }
   }

   let gfx;
   switch (type) {
      case 'suit':
         gfx = suit_gfx;
         break;

      case 'dozer':
         gfx = dozer_gfx;
         break;

      case 'digger':
         gfx = digger_gfx;
         break;

      case 'tanker':
         gfx = tanker_gfx;
         break;
   }

   new_sprite.drawBody = function() {

      // pg.stroke(255,0,0);
      // pg.line(0,0,100*sin(this.target_dir),100*cos(this.target_dir));

      if(this.dir >= 0) {

         let img;
         if(type == 'suit' && this.walk_step < 0) {
            img = gfx[this.dir][5];
         } else {
            img = gfx[this.dir][4];
         }
         pg.image(img, 0,0,img.width * sprite_scale, img.height * sprite_scale);

         for(let i=0; i<this.linha_wrap; i++) {
            img = gfx[this.dir][this.novelo_gfx[i]];
            pg.image(img, 0,0,img.width * sprite_scale, img.height * sprite_scale);
         }

      } else {

         // let w = this.width;
         // let h = this.height;
         // pg.fill(stats_light);
         // pg.ellipse(0,0,w, h);
         // pg.stroke(stats_dark);
         // pg.noFill();
         // pg.ellipse(0,0,w, h);
      }
   }


   switch (type) {
      case 'suit':
         initSuit(new_sprite);
         suits.add(new_sprite);
         break;
      case 'dozer':
         initDozer(new_sprite);
         dozers.add(new_sprite);
         break;
      case 'digger':
         initDigger(new_sprite);
         diggers.add(new_sprite);
         break;
      case 'tanker':
         initTanker(new_sprite);
         tankers.add(new_sprite);
         break;
   }
}


function initSuit(sprite) {
   sprite.width = suit_width;
   sprite.height = suit_height;
   sprite.maxSpeed = suit_max_speed;
   sprite.slowdown = suit_slowdown;
   sprite.slowdown = suit_slowdown;
   sprite.cost_rate = suit_cost_rate;
   sprite.cost_max = suit_cost_max;
}
function initDozer(sprite) {
   sprite.width = dozer_size;
   sprite.height = dozer_size;
   sprite.setCollider("circle");
   sprite.maxSpeed = dozer_max_speed;
   sprite.slowdown = dozer_slowdown;
   sprite.cost_rate = dozer_cost_rate;
   sprite.cost_max = dozer_cost_max;
}
function initDigger(sprite) {
   sprite.width = digger_size;
   sprite.height = digger_size;
   sprite.setCollider("circle");
   sprite.maxSpeed = digger_max_speed;
   sprite.slowdown = digger_slowdown;
   sprite.cost_rate = digger_cost_rate;
   sprite.cost_max = digger_cost_max;
}
function initTanker(sprite) {
   sprite.width = tanker_size;
   sprite.height = tanker_size;
   sprite.setCollider("circle");
   sprite.maxSpeed = tanker_max_speed;
   sprite.slowdown = tanker_slowdown;
   sprite.cost_rate = tanker_cost_rate;
   sprite.cost_max = tanker_cost_max;
}



function initPosition() {
   let pos = createVector();
   let dirs = ['n', 's', 'e', 'w'];
   let dir = random(dirs);
   if(level == 3 && dir == 'e') {
      dir = random(dirs);
      // make east spawn less likely on cabo delgado map
   }
   let m = 10;
   let w, h1, h2;
   switch(dir) {
      case 'n':
         w = (level == 3) ? pg.width - 200 : pg.width;
         pos.x = random(0,w);
         pos.y = -m;
         break;
      case 's':
         w = (level == 3) ? pg.width - 250 : pg.width;
         pos.x = random(0,w);
         pos.y = pg.height + m;
         break;
      case 'e':
         pos.x = pg.width + m;
         h1 = (level == 3) ? 170 : 0;
         h2 = (level == 3) ? pg.height - 250 : pg.height;
         pos.y = random(h1,h2);
         break;
      case 'w':
         pos.x = -m;
         pos.y = random(0,pg.height);
         break;
   }
   return pos;
}


// ----------------------------------------------------------------- //
//                               SCENE
// ----------------------------------------------------------------- //

function initCenas() {

   for(let i=0; i<cena_count; i++) {

      let prop = pg.createSprite(0,0,50,50);
      prop.img;
      prop.immovable = true;

      prop.draw = function() {
         pg.image(this.img, 0,0,this.img.width * sprite_scale, this.img.height * sprite_scale);
         // pg.fill(green_dark);
         // pg.ellipse(0,0,this.width, this.height);
      };
      cenas.add(prop);
   }
}


function placeCenas() {

   for(let i=0; i<cenas.length; i++) {

      let x = 0;
      let y = 0;
      let m = 100;

      while(
         dist(target.position.x, target.position.y, x, y) < 200 ||
         x < m || x > pg.width-m ||
         y < m || y > pg.height-m ||
         (level==3 && dist(pg.width, 0, x, y) < 100) ||
         (level==3 && dist(pg.width, pg.height, x, y) < 250)
         ) {
         x = random(0,pg.width);
         y = random(0,pg.height);
         for(let j = 0; j<cenas.length; j++) {
            if(j!= i) {
               if(dist(x,y, cenas[j].position.x, cenas[j].position.y) < 150) {
                  x = 0;
                  y = 0;
               }
            }
         }
      }
      cenas[i].position.x = x;
      cenas[i].position.y = y;

      let type = round(random(0,num_cenas-1));
      cenas[i].img = cena_gfx[type];

      let d = cena_size[type];
      cenas[i].setCollider("circle",0,0,d/2);
      cenas[i].width = d;
      cenas[i].height = d;

   }
}


function drawStats() {

   let w = 5;
   let mx = 10;
   let my = 20;
   let h = 110;
   let hb = (bolsa/bolsa_init)*h;
   let hl = (linha/linha_total)*h;

   pg.fill(linha_red_light);
   pg.noStroke();
   pg.rect(mx,my+(h-hl),w,hl);
   // pg.textSize(16);
   // pg.textAlign(LEFT, TOP);
   // pg.text('linha:\n'+linha+'/'+linha_total,m,100+2*m);
   pg.stroke(linha_red);
   pg.noFill();
   pg.rect(mx,my,w,h);

   pg.noStroke();
   pg.fill(stats_dark);
   pg.rect(pg.width-(mx+w),my,w,h);
   // pg.textSize(16);
   // pg.textAlign(RIGHT, TOP);
   // pg.text('bolsa:\n'+bolsa,width-m,100+2*m);
   pg.noStroke();
   pg.fill(stats_light);
   pg.rect(pg.width-(mx+w),my+(h-hb),w,hb);
   pg.stroke(stats_mid);
   pg.noFill();
   pg.rect(pg.width-(mx+w),my,w,h);
}


// ----------------------------------------------------------------- //
//                            INTERACTION
// ----------------------------------------------------------------- //

function mousePressed() {

   if(game_state == 1) {

      let click_dist = 20;
      let x = mouseX;
      let y = mouseY;
      for(let i = 0; i<suits.length; i++) {
         if(dist(x, y, suits[i].position.x, suits[i].position.y) < click_dist) {
            suits[i].hit();
         }
      }
      for(let i = 0; i<dozers.length; i++) {
         if(dist(x, y, dozers[i].position.x, dozers[i].position.y) < click_dist) {
            dozers[i].hit();
         }
      }
      for(let i = 0; i<diggers.length; i++) {
         if(dist(x, y, diggers[i].position.x, diggers[i].position.y) < click_dist) {
            diggers[i].hit();
         }
      }
      for(let i = 0; i<tankers.length; i++) {
         if(dist(x, y, tankers[i].position.x, tankers[i].position.y) < click_dist) {
            tankers[i].hit();
         }
      }
   }

}


function keyPressed() {

   if (key == '9') {
      //newSprite('suit');
      //placeCenas();
      loseGame();
   }
   if (key == '8') {
      winGame();
   }
}