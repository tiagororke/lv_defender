
let num_perguntas = 1;
let num_correta = 0;
updateProgress(num_perguntas, max_perguntas);

let resposta_certa_id;

let pergunta_ids = new Set()
let perguntas = [];
let pergunta_cats = [];
let pergunta_fontes = [];
let respostas_certas = [];
let respostas_erradas_1 = [];
let respostas_erradas_2 = [];

$.getJSON("./conteudo_quiz.json", (data) => {
   perguntas = data.quiz.map( (item) => {
     return item.pergunta;
   });
   pergunta_cats = data.quiz.map( (item) => {
     return item.categoria;
   });
   pergunta_fontes = data.quiz.map( (item) => {
     return item.fonte;
   });
   respostas_certas = data.quiz.map( (item) => {
     return item.resposta_certa;
   });
   respostas_erradas_1 = data.quiz.map( (item) => {
     return item.resposta_errada_1;
   });
   respostas_erradas_2 = data.quiz.map( (item) => {
     return item.resposta_errada_2;
   });
});

function proximaPergunta() {

   let i = Math.floor(Math.random() * perguntas.length);
   while(pergunta_ids.has(i)) {
      i = Math.floor(Math.random() * perguntas.length);
   }
   pergunta_ids.add(i);

   switch (pergunta_cats[i]) {
      case 'efeito_de_estufa':
         $("#content-bg-img").attr('src', 'img/emissions_sandor-somkuti_CC-BY-SA.jpg');
         $("#photo_credit").text('foto: Sandor Somkuti CC-BY-SA');
         break;
      case 'alterações_climáticas':
      case 'sumidouros_de_carbono':
      case 'degelo':
         $("#content-bg-img").attr('src', 'img/forest_stanley-zimny_CC-BY-NC.jpg');
         $("#photo_credit").text('foto: Stanley Zimny CC-BY-NC');
         break;
      case 'combustíveis_fósseis':
         $("#content-bg-img").attr('src', 'img/deepwater-horizon-spill_dvidshub_CC-BY.jpg');
         $("#photo_credit").text('foto: dvidshub CC-BY');
         break;
      case 'sectores_mais_poluentes':
         $("#content-bg-img").attr('src', 'img/smoke_tom-woodward_CC-BY-SA.jpg');
         $("#photo_credit").text('foto: Tom Woodward CC-BY-SA');
         break;
      case 'acordo_de_paris':
      case 'justiça_climática':
         $("#content-bg-img").attr('src', 'img/greve-estudantil.jpg');
         $("#photo_credit").text('-');
         break;
      case 'soluções':
         $("#content-bg-img").attr('src', 'img/camp-in-gas.jpg');
         $("#photo_credit").text('-');
         break;
      case 'acordo_de_glasgow':
         $("#content-bg-img").attr('src', 'img/parar-o-furo.jpg');
         $("#photo_credit").text('-');
         break;
   }

   $("#pergunta").text(perguntas[i]);
   $("#fonte").attr("href", pergunta_fontes[i]);
   $("#correta_msg_fonte").attr("href", pergunta_fontes[i]);
   $("#errada_msg_fonte").attr("href", pergunta_fontes[i]);
   $("#resposta_certa").text(respostas_certas[i]);

   let respostas = [
      respostas_certas[i],
      respostas_erradas_1[i],
      respostas_erradas_2[i]
   ]
   respostas = shuffle(respostas);
   $("#resposta0").text(respostas[0]);
   $("#resposta1").text(respostas[1]);
   $("#resposta2").text(respostas[2]);
   for(let h=0; h<3; h++) {
      if(respostas[h] === respostas_certas[i]) {
         resposta_certa_id = h;
      }
   }

}

function updateProgress(n,t) {
   let p;
   if(n != null) {
      p = n * 100/(t+1);
      if(n > t) {
         $("#progress").text("Quiz Completo");
      } else {
         $("#progress").text("Pergunta " + n + " de " + t);
      }
   } else {
      p = 100;
   }
   $("#progress").attr("style","width: " + p + "%");
}


$("#start").on("click", start);
function start() {
   $("#bem_vindo").attr("style","display: none");
   $("#quiz").attr("style","display: ''");
   proximaPergunta();
}

$("#resposta0").on("click", check.bind(null,0));
$("#resposta1").on("click", check.bind(null,1));
$("#resposta2").on("click", check.bind(null,2));
function check(h) {
   if (h == resposta_certa_id) {
      num_correta++;
      $("#correta").attr("style","display: ''");
   } else {
      $("#errada").attr("style","display: ''");
   }
}

$("#correta_proxima").on("click", proxima);
$("#errada_proxima").on("click", proxima);
function proxima() {
   num_perguntas++;
   $("#correta").attr("style","display: none");
   $("#errada").attr("style","display: none");
   if(num_perguntas > max_perguntas) {
      updateProgress(num_perguntas, max_perguntas);
      $("#num_correta").text(num_correta);
      $("#num_perguntas").text(max_perguntas);
      $("#terminou").attr("style","display: ''");
   } else {
      updateProgress(num_perguntas, max_perguntas);
      proximaPergunta();
   }
}

// from https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array
function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}