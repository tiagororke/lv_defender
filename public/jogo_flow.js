
let local_title = [];
let local_info = [];
let perder_msgs = [];
let perder_msg_fontes = [];
let ganhar_msg = [];
let ganhar_next = [];

$.getJSON("./conteudo_jogo.json", (data) => {
   local_title = data.local.map( (item) => {
     return item.title;
   });
   local_info = data.local.map( (item) => {
     return item.info;
   });
   perder_msgs = data.perder.map( (item) => {
     return item.msg;
   });
   perder_msg_fontes = data.perder.map( (item) => {
     return item.fonte;
   });
   ganhar_msg = data.ganhar.map( (item) => {
     return item.msg;
   });
   ganhar_next = data.ganhar.map( (item) => {
     return item.next;
   });

   // set background and center on target coordinates
   switch (local) {
      case 'queensland':
         $('body').css('background-image', 'url(assets/mapa_queensland.jpg)');
         $('body').css('background-position', 'calc(100vw/2 - 872px) calc(481px - 776px')
         $("#heading").text("Nível 1: " + local_title[0]);
         $("#local_title").text(local_title[0]);
         $("#local_img").attr('src', 'assets/cena/queensland/cena4.png');
         $("#local_info").text(local_info[0]);
         $("#ganhar_msg").text(ganhar_msg[0]);
         $("#ganhar_next").text(ganhar_next[0]);
         $("#perder_img").attr('src', 'assets/target/adani/target_working_small.png');
         $("#ganhar_img").attr('src', 'assets/target/adani/target_blocked_small.png');
         break;
      case 'vaca_muerta':
         $('body').css('background-image', 'url(assets/mapa_vaca_muerta.jpg)');
         $('body').css('background-position', 'calc(100vw/2 - 960px) calc(481px - 960px')
         $("#heading").text("Nível 2: " + local_title[1]);
         $("#local_title").text(local_title[1]);
         $("#local_img").attr('src', 'assets/cena/vaca_muerta/cena4.png');
         $("#local_info").text(local_info[1]);
         $("#ganhar_msg").text(ganhar_msg[1]);
         $("#ganhar_next").text(ganhar_next[1]);
         $("#perder_img").attr('src', 'assets/target/exxon/target_working_crop.png');
         $("#ganhar_img").attr('src', 'assets/target/exxon/target_blocked.png');
         break;
      case 'cabo_delgado':
         $('body').css('background-image', 'url(assets/mapa_cabo_delgado.jpg)');
         $('body').css('background-position', 'calc(100vw/2 - 988px) calc(481px - 988px')
         $("#heading").text("Nível 3: " + local_title[2]);
         $("#local_title").text(local_title[2]);
         $("#local_img").attr('src', 'assets/cena/cabo_delgado/cena1.png');
         $("#local_info").text(local_info[2]);
         $("#ganhar_msg").text(ganhar_msg[2]);
         $("#ganhar_next").text(ganhar_next[2]);
         $("#perder_img").attr('src', 'assets/target/total/target_working.png');
         $("#ganhar_img").attr('src', 'assets/target/total/target_blocked.png');
         break;
   }
});


$("#jogar").on("click", () => {
   $("#bem_vindo").attr("style","display: none");
   if(guia) {
      $("#como_jogar").attr("style","display: ''");
   } else {
      $("#skip").attr("style","display: ''");
      initTimers();
      game_state = 1;
   }
});
$("#continuar").on("click", () => {
   $("#como_jogar").attr("style","display: none");
   $("#skip").attr("style","display: ''");
   initTimers();
   game_state = 1;
});
$("#tentar_denovo").on("click", () => {
   $("#perdeste").attr("style","display: none");
   placeCenas();
   initTimers();
   game_state = 1;
});

function perdeste() {
   let i = Math.floor(Math.random() * perder_msgs.length);
   $("#perdeste_msg").text(perder_msgs[i]);
   $("#perdeste_msg_fonte").attr("href",perder_msg_fontes[i]);
   $("#perdeste").attr("style","display: ''");
}

function ganhaste() {
   $("#ganhaste").attr("style","display: ''");
}
