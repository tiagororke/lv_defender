
// ----------------------------------------------------------------- //
//                            CONFIG
// ----------------------------------------------------------------- //

// game graphics buffer --------------------------------------------

let game_width = 800;
let game_height = 600;
let game_y = 200;
let game_x; // defined in setup


// colors ----------------------------------------------------------

let background_color     = '#E6A774';
let sand                 = '#c7a953';
let green_light          = '#3B7845';
let green_dark           = '#014819';
let linha_red            = '#9a0c0c';
let linha_red_light      = '#f30c0c';
let australis_green      = '#00ac9f';
let australis_blue       = '#306fb7';
let australis_blue_dark  = '#002b51';


// gameplay --------------------------------------------------------

let continue_wait = 1300;
let continue_timer = 0;

let linha_init = 20;           // initial linha qty
let bolsa_init = 3000;         // initial bolsa qty
let linha_total = linha_init;  // increases over time?

let tick = 2000; // cost update interval millis


// map coordinates -------------------------------------------------

let start_x = 950;
let start_y = 950;

let batalha_x = 705;
let batalha_y = 1294;

let pombal_x = 1014;
let pombal_y = 535;


// decor -----------------------------------------------------------

let novelo_x = 70;
let novelo_y = 75;
let pasta_x = 85; // ( = width - pasta_X )
let pasta_y = 80;

const num_cenas = 7;  // number of different props available
const cena_count = 7; // number of props to draw

// scenery element sizes
const cena_size = [73, 90, 60, 50, 66, 50, 63];

// scale factor for all sprite graphics
const sprite_scale = 0.5;


// sprites ---------------------------------------------------------

let suit_timer_init = 0;
let dozer_timer_init = 10000;
let digger_timer_init = 15000;
let tanker_timer_init = 30000;
let suit_interval = 4000;
let dozer_interval = 7000;
let digger_interval = 10000;
let tanker_interval = 15000;

let suit_slowdown = 0.2; // affects maxSpeed
let suit_max_speed = 0.8;
let suit_cost_rate = 2; // "cost per tick"
let suit_cost_max = 20;
let suit_width = 20;
let suit_height = 30;
let suit_turn_timeout = 1000;

let dozer_slowdown = 0.13;
let dozer_max_speed = 0.6;
let dozer_cost_rate = 3;
let dozer_cost_max = 60;
let dozer_size = 35;
let dozer_turn_timeout = 1000;

let digger_slowdown = 0.08;
let digger_max_speed = 0.4;
let digger_cost_rate = 5;
let digger_cost_max = 80;
let digger_size = 45;
let digger_turn_timeout = 1000;

let tanker_slowdown = 0.04;
let tanker_max_speed = 0.2;
let tanker_cost_rate = 8;
let tanker_cost_max = 200;
let tanker_size = 55;
let tanker_turn_timeout = 1000;


// ----------------------------------------------------------------- //
//                               VARS
// ----------------------------------------------------------------- //

// game graphics buffer
let pg;

let font;

let game_state = 0;
/*
   0 = welcome
   1 = how to play?
   2 = welcome to bajouca
   3 = playing level 1
   4 = lost level 2
   5 = won level 2
   6 = welcome to aljubarrota
   7 = playing level 2
   8 = lost level 2
   9 = won level 2
*/

// background
let mapa;
let mapa_x;
let mapa_y;
let mapa_location = 0;
   // 0 start
   // 1 batalha
   // 2 pombal

// avialable linha
let linha = linha_total;

// remaining bolsa
let bolsa = bolsa_init;

// sprite groups
let torre;
let cenas;
let suits;
let dozers;
let diggers;
let tankers;

// sprite timers
let suit_timer;
let dozer_timer;
let digger_timer;
let tanker_timer;

// sprite graphics
// each skin has four directions, with one skin and four wrap overlays
let dozer_gfx = Array.from(Array(4), () => new Array(5));
let digger_gfx = Array.from(Array(4), () => new Array(5));
let tanker_gfx = Array.from(Array(4), () => new Array(5));
let suit_gfx = Array.from(Array(4), () => new Array(6)); // suits have two walking frames

// decor graphics
let torre_gfx;
let torre_fracking;
let torre_linha;
let novelo_gfx = new Array(5);
let pasta_gfx = new Array(6);
let cena_gfx = new Array(num_cenas);


// ----------------------------------------------------------------- //
//                          SETUP & PRELOAD
// ----------------------------------------------------------------- //

function preload() {

   font = loadFont('assets/AnonymousPro-Bold.ttf');
   mapa = loadImage('assets/mapa_portugal.jpg');

   torre_gfx = loadImage('assets/torre/torre.png');
   torre_fracking = loadImage('assets/torre/torre_fracking.png');
   torre_linha = loadImage('assets/torre/torre_linha.png');

   for(let i=1; i<=novelo_gfx.length; i++) {
      novelo_gfx[i-1] = loadImage('assets/linha/linha' + i + '.png');
   }
   for(let i=1; i<=pasta_gfx.length; i++) {
      pasta_gfx[i-1] = loadImage('assets/pasta/australis/pasta' + i + '.png');
   }
   for(let i=1; i<=cena_gfx.length; i++) {
      cena_gfx[i-1] = loadImage('assets/cena/portugal/cena' + i + '.png');
   }

   let dir = ['nw', 'ne', 'se', 'sw'];

   for(let i=0; i<4; i++) {
      d = dir[i];
      digger_gfx[i][4] = loadImage('assets/digger/australis/digger_' + d + '.png');
      dozer_gfx[i][4]  = loadImage('assets/dozer/australis/dozer_'   + d + '.png');
      tanker_gfx[i][4] = loadImage('assets/tanker/australis/tanker_' + d + '.png');
      suit_gfx[i][4]   = loadImage('assets/suit/australis/suit_'     + d + '1.png');
      suit_gfx[i][5]   = loadImage('assets/suit/australis/suit_'     + d + '2.png');

      for(let j=1; j<=4; j++) {
         digger_gfx[i][j-1] = loadImage('assets/digger/digger_' + d + '_wrap' + j + '.png');
         dozer_gfx[i][j-1]  = loadImage('assets/dozer/dozer_'   + d + '_wrap' + j + '.png');
         tanker_gfx[i][j-1] = loadImage('assets/tanker/tanker_' + d + '_wrap' + j + '.png');
         suit_gfx[i][j-1]   = loadImage('assets/suit/suit_'     + d + '_wrap' + j + '.png');
      }
   }
}


function setup() {

   let myCanvas = createCanvas(windowWidth, 2500);
   myCanvas.parent('jogo');
   $("#jogo-overlay").attr("style","visibility: hidden;");

   pg = createGraphics(game_width, game_height);
   pg.textFont(font);
   pg.strokeWeight(2);

   game_x = windowWidth/2 - game_width/2;

   torre = pg.createSprite(pg.width/2, pg.height/2, 50, 50);
   torre.setCollider("circle");
   torre.draw = function() {
      // pg.fill(sand);
      // pg.ellipse(0,0,this.width*2, this.height*2);
      // pg.fill(100);
      // pg.triangle(-20,20,0,-40,20,20);
      let gfx;
      switch(game_state) {
         case 1:
            gfx = torre_gfx;
            break;
         case 2:
            gfx = torre_fracking;
            break;
         case 3:
            gfx = torre_linha;
            break;
      }
      pg.image(gfx, 0,0,torre_gfx.width * sprite_scale, torre_gfx.height * sprite_scale);

   }

   novelo = pg.createSprite(novelo_x, novelo_y, 50, 50);
   novelo.draw = function() {
      let l = round(map(linha, linha_total, 0, 0, novelo_gfx.length-2));
      if(linha <= 0) {
         l = novelo_gfx.length-1;
      }
      pg.image(novelo_gfx[l], 0,0,novelo_gfx[l].width * sprite_scale, novelo_gfx[l].height * sprite_scale);
   }

   pasta = pg.createSprite(pg.width - pasta_x, pasta_y, 50, 50);
   pasta.draw = function() {
      let l = round(map(bolsa, bolsa_init, 0, pasta_gfx.length-1, 1));
      if(bolsa <= 0) {
         l = 0;
      }
      pg.image(pasta_gfx[l], 0,0,pasta_gfx[l].width * sprite_scale, pasta_gfx[l].height * sprite_scale);
   }

   init();
   initGame();
}

function windowResized() {
   resizeCanvas(windowWidth, windowHeight);
   game_x = windowWidth/2 - game_width/2;
}


// ----------------------------------------------------------------- //
//                            MAIN LOOP
// ----------------------------------------------------------------- //

function draw() {

   background(background_color);

   for(var i=0; i<allSprites.length; i++) {
      allSprites[i].depth = allSprites[i].position.y;
   }
   novelo.depth = 1000;
   pasta.depth = 1000;

   switch(mapa_location) {
      case 0:
         mapa_x = -start_x + windowWidth/2;
         mapa_y = -start_y + game_y + game_height/2;
         break;
      case 1:
         mapa_x = -batalha_x + windowWidth/2;
         mapa_y = -batalha_y + game_y + game_height/2;
         break;
      case 2:
         mapa_x = -pombal_x + windowWidth/2;
         mapa_y = -pombal_y + game_y + game_height/2;
         break;
   }

   image(mapa, mapa_x, mapa_y);

   pg.background(green_light);
   pg.clear();

   switch(game_state) {

      case 0: // welcome
         pg.fill(0);
         pg.noStroke();
         pg.textSize(32);
         pg.textAlign(CENTER, CENTER);
         pg.text('welcome\nclick to play', pg.width/2, pg.height/2);
         break;

      case 1: // playing

         if(millis() - suit_timer > suit_interval) {
            suit_timer = millis();
            newSprite('suit');
         }
         if(millis() - dozer_timer > dozer_interval) {
            dozer_timer = millis();
            newSprite('dozer');
         }
         if(millis() - digger_timer > digger_interval) {
            digger_timer = millis();
            newSprite('digger');
         }
         if(millis() - tanker_timer > tanker_interval) {
            tanker_timer = millis();
            newSprite('tanker');
         }

         for(let i = 0; i<suits.length; i++) {
            suits[i].doBusiness();
         }
         for(let i = 0; i<dozers.length; i++) {
            dozers[i].doBusiness();
         }
         for(let i = 0; i<diggers.length; i++) {
            diggers[i].doBusiness();
         }
         for(let i = 0; i<tankers.length; i++) {
            tankers[i].doBusiness();
         }
         suits.bounce(cenas);
         dozers.bounce(cenas);
         diggers.bounce(cenas);
         tankers.bounce(cenas);
         drawSprites();
         drawStats();

         break;

      case 2: // lost
         pg.fill(0);
         pg.noStroke();
         pg.textSize(32);
         pg.textAlign(CENTER, CENTER);
         if(millis() - continue_timer < continue_wait) {
            pg.text('you lost!', pg.width/2, pg.height/3);
         } else {
            pg.text('you lost!\nclick to play again', pg.width/2, pg.height/3);
         }
         drawSprites();
         drawStats();
         break;

      case 3: // won
         pg.fill(0);
         pg.noStroke();
         pg.textSize(32);
         pg.textAlign(CENTER, CENTER);
         if(millis() - continue_timer < continue_wait) {
            pg.text('you won!', pg.width/2, pg.height/3);
         } else {
            pg.text('you won!\nclick to play again', pg.width/2, pg.height/3);
         }
         drawSprites();
         drawStats();
         break;

   }
   //image(pg, 0,0);
   image(pg, game_x, game_y);

   stroke(50,100);
   drawingContext.setLineDash([20, 20]);
   strokeWeight(5);
   noFill();
   rect(game_x,game_y,pg.width,pg.height);
   drawingContext.setLineDash([]);

}


// ----------------------------------------------------------------- //
//                            GAME STATE
// ----------------------------------------------------------------- //

function init() {
   cenas = new Group();
   suits = new Group();
   dozers = new Group();
   diggers = new Group();
   tankers = new Group();
   initCenas();
}

function initGame() {
   suits.removeSprites();
   suits.clear();
   dozers.removeSprites();
   dozers.clear();
   diggers.removeSprites();
   diggers.clear();
   tankers.removeSprites();
   tankers.clear();
   linha_total = linha_init;
   linha = linha_total;
   bolsa = bolsa_init;
   placeCenas();
   suit_timer = suit_timer_init;
   dozer_timer = dozer_timer_init;
   digger_timer = digger_timer_init;
   tanker_timer = tanker_timer_init;
}

function loseGame() {
   game_state = 2;
   continue_timer = millis();
}

function winGame() {
   game_state = 3;
   continue_timer = millis();
}


// ----------------------------------------------------------------- //
//                              SPRITES
// ----------------------------------------------------------------- //

function newSprite(type) {

   let new_sprite = pg.createSprite(0,0, 10,10);
   new_sprite.position = initPosition();
   new_sprite.linha_wrap = 0; // counts the number of times the sprite has been clicked
   new_sprite.novelo_gfx = shuffle(new Array(0,1,2,3)); // random order for applies wrap textures
   new_sprite.cost = 0;
   new_sprite.tick_timer = 0;
   new_sprite.ticker = 0;

   // cardinal direction for choosing graphic
   // 0 nw, 1 ne, 2 se, 3 sw
   new_sprite.dir = 0;
   new_sprite.pdir = 0;

   // angle to tower (not used by suits)
   new_sprite.target_dir = 0;

   // to prevent direction flickering back and forth
   // minimum time between direction changes (set by turn timeout)
   new_sprite.turn_latch = false;
   new_sprite.turn_timer = 0;


   if(type == 'suit') {
      new_sprite.walk_step = 1; // for switching frames on walking animatiosn
      new_sprite.walk_timer = 0;
      new_sprite.walk_time = 200;
   }


   new_sprite.hit = function() {
      if(this.linha_wrap < 4 && linha > 0) {
         linha--;
         this.linha_wrap++;
         this.maxSpeed -= this.slowdown;
         if(this.maxSpeed <= 0.01) {
            this.maxSpeed = 0;
            this.rotation = 90;
            this.walk_time = 500;
         }
      }
   };

   new_sprite.draw = function() {
      if(game_state == 1) {
         this.drawBody();

         // let w = this.width;
         // let h = this.height;
         // pg.stroke(linha_red_light);
         // for(let i=0; i<this.linha_wrap; i++) {
         //    pg.line(-(w/2 + 5),i*5,w/2 + 5,i*5 - 10);
         // }

         // ticker
         if(this.ticker > 0) {
            pg.fill(0,172,159,this.ticker);
            pg.noStroke();
            pg.textSize(30);
            pg.textAlign(CENTER,BOTTOM);
            pg.text('€',0,-0.15*(255-this.ticker) - 10);
            this.ticker -= 5;
            if(this.ticker<0) {
               this.ticker = 0;
            }
         }

         // health stats
         // pg.fill(australis_green);
         // pg.noStroke();
         // pg.textSize(14);
         // pg.textAlign(CENTER,BOTTOM);
         // pg.text(this.cost+'/'+this.cost_max,0,30);
      }
   };

   new_sprite.doBusiness = function() {


      if(!this.turn_latch) {

         // for finding the right direction
         let a;

         if(type == 'suit') {
            // suits just use the attraction point, moving at any angle
            this.attractionPoint(0.2, torre.position.x, torre.position.y);
            a = this.getDirection();
         } else {
            // vehicles travel only at 45 degrees
            // so we store the direction to the tower in target_dir
            this.target_dir = atan2(
               torre.position.x - this.position.x, 
               torre.position.y - this.position.y
            );
            // convert to degrees as is used in p5.play
            a = -360 * (this.target_dir - PI/2)/TWO_PI;
            this.setSpeed(this.maxSpeed, a);
         }

         // choose cardinal direction based on angle to tower
         if (a < -180) {
            a += 360;
         }
         if (a >= 180) {
            a -= 360;
         }
         if(a >= -180 && a < -90) {
            this.dir = 0;
         } else if(a >= -90 && a < 0) {
            this.dir = 1;
         } else if(a >= 0 && a < 90) {
            this.dir = 2;
         } else if(a >= 90 && a < 180) {
            this.dir = 3;
         } else {
            this.dir = -1;
         }

         // set movement direction for vehicles based on cardinal direction
         if(type != 'suit') {
            this.setSpeed(this.maxSpeed, 45 + (this.dir*90 - 180));
         }

         // set turn latch when direction changes
         if(this.pdir != this.dir) {
            this.turn_latch = true;
            this.turn_timer = millis();
            this.pdir = this.dir;
         }

      } else {

         let t;
         switch (type) {
            case 'suit':
               t = suit_turn_timeout;
               break;

            case 'dozer':
               t = dozer_turn_timeout;
               break;

            case 'digger':
               t = digger_turn_timeout;
               break;

            case 'tanker':
               t = tanker_turn_timeout;
               break;
         }

         if(millis() - this.turn_timer > t) {
            this.turn_latch = false;
         }

      }

      this.collide(torre, loseGame);
      if(millis() - this.tick_timer > tick) {
         this.tick_timer = millis();
         this.ticker = 255;
         this.cost += this.cost_rate;
         bolsa -= this.cost_rate;
         if(bolsa<=0) {
            winGame();
         }
         if(this.cost > this.cost_max) {
            linha += this.linha_wrap;
            this.remove();
         }
      }

      if(type == 'suit') { // && this.linha_wrap < 4) {
         if(millis() - this.walk_timer > this.walk_time) {
            this.walk_step *= -1;
            this.walk_timer = millis();
         }
      }
   }

   let gfx;
   switch (type) {
      case 'suit':
         gfx = suit_gfx;
         break;

      case 'dozer':
         gfx = dozer_gfx;
         break;

      case 'digger':
         gfx = digger_gfx;
         break;

      case 'tanker':
         gfx = tanker_gfx;
         break;
   }

   new_sprite.drawBody = function() {

      // pg.stroke(255,0,0);
      // pg.line(0,0,100*sin(this.target_dir),100*cos(this.target_dir));

      if(this.dir >= 0) {

         let img;
         if(type == 'suit' && this.walk_step < 0) {
            img = gfx[this.dir][5];
         } else {
            img = gfx[this.dir][4];
         }
         pg.image(img, 0,0,img.width * sprite_scale, img.height * sprite_scale);

         for(let i=0; i<this.linha_wrap; i++) {
            img = gfx[this.dir][this.novelo_gfx[i]];
            pg.image(img, 0,0,img.width * sprite_scale, img.height * sprite_scale);
         }

      } else {

         // let w = this.width;
         // let h = this.height;
         // pg.fill(australis_green);
         // pg.ellipse(0,0,w, h);
         // pg.stroke(australis_blue_dark);
         // pg.noFill();
         // pg.ellipse(0,0,w, h);
      }
   }


   switch (type) {
      case 'suit':
         initSuit(new_sprite);
         suits.add(new_sprite);
         break;
      case 'dozer':
         initDozer(new_sprite);
         dozers.add(new_sprite);
         break;
      case 'digger':
         initDigger(new_sprite);
         diggers.add(new_sprite);
         break;
      case 'tanker':
         initTanker(new_sprite);
         tankers.add(new_sprite);
         break;
   }
}


function initSuit(sprite) {
   sprite.width = suit_width;
   sprite.height = suit_height;
   sprite.maxSpeed = suit_max_speed;
   sprite.slowdown = suit_slowdown;
   sprite.slowdown = suit_slowdown;
   sprite.cost_rate = suit_cost_rate;
   sprite.cost_max = suit_cost_max;
}
function initDozer(sprite) {
   sprite.width = dozer_size;
   sprite.height = dozer_size;
   sprite.setCollider("circle");
   sprite.maxSpeed = dozer_max_speed;
   sprite.slowdown = dozer_slowdown;
   sprite.cost_rate = dozer_cost_rate;
   sprite.cost_max = dozer_cost_max;
}
function initDigger(sprite) {
   sprite.width = digger_size;
   sprite.height = digger_size;
   sprite.setCollider("circle");
   sprite.maxSpeed = digger_max_speed;
   sprite.slowdown = digger_slowdown;
   sprite.cost_rate = digger_cost_rate;
   sprite.cost_max = digger_cost_max;
}
function initTanker(sprite) {
   sprite.width = tanker_size;
   sprite.height = tanker_size;
   sprite.setCollider("circle");
   sprite.maxSpeed = tanker_max_speed;
   sprite.slowdown = tanker_slowdown;
   sprite.cost_rate = tanker_cost_rate;
   sprite.cost_max = tanker_cost_max;
}



function initPosition() {
   let pos = createVector();
   let dirs = ['n', 's', 'e', 'w'];
   let dir = random(dirs);
   let m = 20;
   switch(dir) {
      case 'n':
         pos.x = random(0,pg.width);
         pos.y = -m;
         break;
      case 's':
         pos.x = random(0,pg.width);
         pos.y = pg.height + m;
         break;
      case 'e':
         pos.x = -m;
         pos.y = random(0,pg.height);
         break;
      case 'w':
         pos.x = pg.width + m;
         pos.y = random(0,pg.height);
         break;
   }
   return pos;
}


// ----------------------------------------------------------------- //
//                               SCENE
// ----------------------------------------------------------------- //

function initCenas() {

   for(let i=0; i<cena_count; i++) {

      let prop = pg.createSprite(0,0,50,50);
      prop.img;
      prop.immovable = true;

      prop.draw = function() {
         pg.image(this.img, 0,0,this.img.width * sprite_scale, this.img.height * sprite_scale);
         // pg.fill(green_dark);
         // pg.ellipse(0,0,this.width, this.height);
      };
      cenas.add(prop);
   }
}


function placeCenas() {

   for(let i=0; i<cenas.length; i++) {

      let x = 0;
      let y = 0;
      let m = 100;

      while(
         dist(torre.position.x, torre.position.y, x, y) < 200 ||
         x < m || x > pg.width-m ||
         y < m || y > pg.height-m
         ) {
         x = random(0,pg.width);
         y = random(0,pg.height);
         for(let j = 0; j<cenas.length; j++) {
            if(j!= i) {
               if(dist(x,y, cenas[j].position.x, cenas[j].position.y) < 150) {
                  x = 0;
                  y = 0;
               }
            }
         }
      }
      cenas[i].position.x = x;
      cenas[i].position.y = y;

      let type = round(random(0,num_cenas-1));
      cenas[i].img = cena_gfx[type];

      let d = cena_size[type];
      cenas[i].setCollider("circle",0,0,d/2);
      cenas[i].width = d;
      cenas[i].height = d;

   }
}


function drawStats() {

   let w = 5;
   let mx = 10;
   let my = 20;
   let h = 110;
   let hb = (bolsa/bolsa_init)*h;
   let hl = (linha/linha_total)*h;

   pg.fill(linha_red_light);
   pg.noStroke();
   pg.rect(mx,my+(h-hl),w,hl);
   // pg.textSize(16);
   // pg.textAlign(LEFT, TOP);
   // pg.text('linha:\n'+linha+'/'+linha_total,m,100+2*m);
   pg.stroke(linha_red);
   pg.noFill();
   pg.rect(mx,my,w,h);

   pg.noStroke();
   pg.fill(australis_blue_dark);
   pg.rect(pg.width-(mx+w),my,w,h);
   // pg.textSize(16);
   // pg.textAlign(RIGHT, TOP);
   // pg.text('bolsa:\n'+bolsa,width-m,100+2*m);
   pg.noStroke();
   pg.fill(australis_green);
   pg.rect(pg.width-(mx+w),my+(h-hb),w,hb);
   pg.stroke(australis_blue);
   pg.noFill();
   pg.rect(pg.width-(mx+w),my,w,h);
}


// ----------------------------------------------------------------- //
//                            INTERACTION
// ----------------------------------------------------------------- //

function mousePressed() {

   //console.log(mapa_x, mapa_y);

   switch(game_state) {

      case 0: // welcome
         game_state = 1;
         if(random(0,10) > 5) {
            mapa_location = 1;
         } else {
            mapa_location = 2;
         }
         break;

      case 1: // playing
         let click_dist = 20;
         let x = mouseX - game_x;
         let y = mouseY - game_y;
         for(let i = 0; i<suits.length; i++) {
            if(dist(x, y, suits[i].position.x, suits[i].position.y) < click_dist) {
               suits[i].hit();
            }
         }
         for(let i = 0; i<dozers.length; i++) {
            if(dist(x, y, dozers[i].position.x, dozers[i].position.y) < click_dist) {
               dozers[i].hit();
            }
         }
         for(let i = 0; i<diggers.length; i++) {
            if(dist(x, y, diggers[i].position.x, diggers[i].position.y) < click_dist) {
               diggers[i].hit();
            }
         }
         for(let i = 0; i<tankers.length; i++) {
            if(dist(x, y, tankers[i].position.x, tankers[i].position.y) < click_dist) {
               tankers[i].hit();
            }
         }
         break;

      case 2: // lost
      case 3: // won
         if(millis() - continue_timer > continue_wait) {
            game_state = 0;
            initGame();
         }
         break;

   }

}


function keyPressed() {

   if (key == '0') {
      mapa_location = 0;
   }
   if (key == '1') {
      mapa_location = 1;
   }
   if (key == '2') {
      mapa_location = 2;
   }
   if (key == '9') {
      placeCenas();
   }
}